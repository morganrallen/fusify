var async = require("async");
var browserify = require("browserify");
var cp = require("cp").sync;
var execSync = require("child_process").execFileSync;
var fs = require("fs");
var fusify = require("../");
var mkdirp = require("mkdirp");
var path = require("path");
var rimraf = require("rimraf");
var test = require("tape");
var walk = require("walk-sync");

var entry = path.join(__dirname, "sandbox", "entry");
var fixtures = path.join(__dirname, "fixtures");
var mnt = path.join(__dirname, "sandbox", "mnt");

// cleanup test directories
execSync("rm", [ "-R", "-f", path.join(__dirname, "sandbox", "*") ]);
execSync("mkdir", [ "-p", entry ]);
execSync("mkdir", [ "-p", mnt ]);

walk(fixtures).forEach(function(file) {
  var fixtureFile = path.join(fixtures, file);

  console.log(fixtureFile, path.join(entry, file));
  if(fs.statSync(fixtureFile).isDirectory()) {
    mkdirp.sync(path.join(entry, file));
  } else {
    cp(fixtureFile, path.join(entry, file));
  }
});

var fuse;

test(function(t) {
  t.plan(1);

  fuse = fusify({
    debug: false,
    entry: "index.js",
    entryDir: entry,
    mnt: mnt,
    output: "bundle.js"
  });

  setTimeout(function() {
    t.ok(true);
  }, 100);
}, "fusify");

test(function(t) {
  t.plan(2);

  async.parallel({
    mnt: function(done) {
      fs.readFile(path.join(mnt, "index.js"), done);
    },

    entry: function(done) {
      fs.readFile(path.join(entry, "index.js"), done);
    }
  }, function(err, results) {
    t.ok(!err);
    t.equal(results.mnt.toString(), results.entry.toString());
    t.end();
  });
}, "compare files");

test(function(t) {
  t.plan(1);

  var str = "";
  var b = browserify({
    entries: [ path.join(entry, "index.js") ]
  });

  b.bundle().on("data", function(chunk) {
    str += chunk.toString();
  }).on("end", function() {
    fs.readFile(path.join(mnt, "bundle.js"), function(err, content) {
      t.equal(str, content.toString());
      t.end();
    });
  });
}, "compare bundle");

test(function(t) {
  t.plan(1);

  fuse.unmount(mnt, function() {
    t.ok(true);
    t.end();
    process.exit();
  });
}, "umount");
