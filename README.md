# browserify

FUSE filesystem that provides an always up to date browserify bundle.


This project is a crazy whime. It's probably not safe to use, take precautions.

# usage

fusify *source* *destination* *js entry* [*bundle*]

# example
```
fusify ~/devel/project/src/js ~/devel/project/src_mnt index.js bundle.js
```

This assumes that the file `~/devel/project/src/js/index.js` exists and the bundle will be
output to `~/devel/project/src_mnt/bundle.js`
