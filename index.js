var fs = require("fs");
var fuse = require("fuse-bindings")
var pathJoin = require("path").join.bind(require("path"));

var browserify = require("browserify");
var watchify = require("watchify");

var tmp = "/tmp/bundle.js";

module.exports = function(opts) {
  var entry = opts.entry;
  var entryDir = opts.entryDir;
  var output = opts.output;
  var mnt = opts.mnt;
  var debug = opts.debug;

  var b = browserify([ entry ], {
    basedir: entryDir,
    debug: debug
  });

  var bundling = true;
  var bun = "";

  b.on("bundle", function(bundle) {
    console.log(bundle);
    bundle.on("data", function(chunk) {
      console.log(chunk);
      bun += chunk.toString();
    }).on("end", function() {
      bundling = false;
      console.log("bundled!!!!");
      b.emit("bundled");
    });

    bundle.resume();
  });

  b.bundle();

  fuse.mount(mnt, {
    force: true,

    init: function(cb) {
      cb(null);
    },

    readdir: function (dir, cb) {
      console.log("readdir(%s)", pathJoin(entryDir, dir));

      fs.readdir(pathJoin(entryDir, dir), function(err, files) {
        if(!err && dir === "/") {
          files.push(output);
        }
        
        cb(err, files);
      });
    },

    getattr: function (dir, cb) {
      console.log("getattr(%s)", pathJoin(entryDir, dir));

      if(dir === "/" + output) {
        fs.stat(tmp, cb);
      } else {
        fs.lstat(pathJoin(entryDir, dir), function(err, stat) {
          if(err) {
            cb(fuse[err.code] || fuse.ENOSYS);
          } else {
            cb(0, stat);
          }
        });
      }
    },

    open: function (dir, flags, cb) {
      console.log("open(%s, %s)", pathJoin(entryDir, dir), flags);
      fs.open(pathJoin(entryDir, dir), flags, cb);
    },

    read: function (dir, fd, buf, len, pos, cb) {
      console.log("read(%s, %d, %d, %d)", pathJoin(entryDir, dir), fd, len, pos);

      function readBundle() {
        console.log("readBundle");
        fs.readFile(tmp, function(err, contents) {
          buf.write(bun.slice(pos, pos + len), pos, pos + len);
          cb(len);
        });
      }

      if(dir.split("/").pop() === output) {
        if(bundling) {
          console.log("**** deferred read bundle");
          b.once("bundled", readBundle);
        } else {
          readBundle();
        }
      } else {
        fs.read(fd, buf, 0, len, pos, function(err, length, contents) {
          contents.copy(buf, 0, pos, pos + len);
          cb(len);
        });
      }
    },

    write: function (dir, fd, buf, len, pos, cb) {
      console.log("write(%s, %d, %d, %d)", pathJoin(entryDir, dir), fd, len, pos);
      fs.write(fd, buf, 0, len, pos, function(err, length, contents) {
        cb(length);
        bundling = true;

        b.bundle();
      });
    },

    access: function(path, mode, cb) {
      console.log("access(%s, %d)", pathJoin(entryDir, path), mode);

      fs.access(pathJoin(entryDir, path), mode, function(err) {
       cb(err, mode);
      });
    },

    statfs: function(path, cb) {
      console.log("statfs(%s)", path);
      cb(null);
    },

    fgetattr: function(path, fd, cb) {
      console.log("fgetattr(%s, %d)", path, fd);
      if(path === "/" + output) {
        fs.stat(tmp, cb);
      } else {
        fs.fstat(fd, cb);
      }
    },

    flush: function(path, fd, cb) {
      console.log("flush(%s)", path);
      cb(null);
    },

    fsync: function(path, fd, datasync, cb) {
      console.log("fsync(%s)", path);
      cb(null);
    },

    fsyncdir: function(path, fd, datasync, cb) {
      console.log("fsyncdir(%s)", path);
      cb(null);
    },

    truncate: function(path, size, cb) {
      console.log("truncate(%s)", path);
      cb(null);
    },

    ftruncate: function(path, fd, size, cb) {
      console.log("ftruncate(%s)", path);
      cb(null);
    },

    readlink: function(path, cb) {
      console.log("readlink(%s)", path);
      cb(null);
    },

    chown: function(path, uid, gid, cb) {
      console.log("chown(%s)", path);
      cb(null);
    },

    chmod: function(path, mode, cb) {
      console.log("chmod(%s)", path);
      cb(null);
    },

    mknod: function(path, mode, dev, cb) {
      console.log("mknod(%s)", path);
      cb(null);
    },

    setxattr: function(path, name, buffer, length, offset, flags, cb) {
      console.log("setxattr(%s)", path);
      cb(null);
    },

    getxattr: function(path, name, buffer, length, offset, cb) {
      console.log("getxattr(%s)", path);
      cb(null);
    },

    opendir: function(path, flags, cb) {
      cb(null);
    },

    release: function(path, fd, cb) {
      cb(null);
    },

    releasedir: function(path, fd, cb) {
      cb(null);
    },

    create: function(path, mode, cb) {
      console.log("create(%s)", path);
      cb(null);
    },

    utimens: function(path, atime, mtime, cb) {
      console.log("utimens(%s, %s, %s)", pathJoin(entryDir, path), atime, mtime);

      fs.utimes(pathJoin(entryDir, path), atime, mtime, function(err) {
        if(err) throw err;
        cb();
      });
    },

    unlink: function(path, cb) {
      console.log("unlink(%s)", path);
      cb(null);
    },

    rename: function(src, dest, cb) {
      console.log("rename(%s)", path);
      cb(null);
    },

    link: function(src, dest, cb) {
      console.log("link(%s)", path);
      cb(null);
    },

    symlink: function(src, dest, cb) {
      console.log("symlink(%s)", path);
      cb(null);
    },

    mkdir: function(path, mode, cb) {
      console.log("mkdir(%s)", path);
      cb(null);
    },

    rmdir: function(path, cb) {
      console.log("rmdir(%s)", path);
      cb(null);
    },

    destroy: function(cb) {
      console.log("destroy(%s)");
      cb(null);
    }
  })
   
  process.on("SIGINT", exit);

  function toFlags(flags) {
    return flags;
  }

  function exit() {
    fuse.unmount(mnt, function () {
      process.exit()
    })
  }

  return fuse;
};
