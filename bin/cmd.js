var argv = require("minimist")(process.argv.slice(2));
var fs = require("fs");
var path = require("path");
var fusify = require("../");

if(argv._.length < 3) {
  return usage();
}

var entryDir = argv._[0];
var mnt = argv._[1];
var entry = argv._[2];
var output = argv._[3] || "bundle.js";
 
console.log("Entry Dir: %s", entryDir);
console.log("Mount: %s", mnt);
console.log("Entry: %s", entry);
console.log("Output: %s", output);

fusify({
  entryDir: entryDir,
  mnt: mnt,
  entry: entry,
  output: output
})

function usage() {
  fs.createReadStream(path.join(__dirname, "usage.txt")).pipe(process.stdout);
}
